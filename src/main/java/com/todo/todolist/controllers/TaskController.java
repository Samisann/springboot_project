package com.todo.todolist.controllers;


import com.todo.todolist.models.Task;
import com.todo.todolist.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller qui va nous servir à gérer les tâches
 * @Author Sami
 */
@RestController
@RequestMapping("api/v1/")
public class TaskController {
    @Autowired
    private TaskService taskService;


    /**
     * Methode qui va nous servir à récupérer toutes les tâches
     * @return une liste de tâches
     */
    @GetMapping("/tasks")
    public ResponseEntity<List<Task>> getAll()
    {
        return ResponseEntity.ok(taskService.getTasks());
    }

    /**
     * Méthode GET qui va nous servir à récupérer les tâches complétées
     * @return une réponse HTTP avec une liste de tâches complétées, 200 si tout s'est bien passé
     */
    @GetMapping("/taskCompleted")
    public ResponseEntity<List<Task>> getAllComplete()
    {
        return ResponseEntity.ok(taskService.findAllCompletedTask());
    }

    /**
     * Méthode GET qui va nous servir à récupérer les tâches non complétées
     * @return une réponse HTTP avec une liste de tâches non complétées
     */
    @GetMapping("/taskUncompleted")
    public ResponseEntity<List<Task>> getAllUncomplete()
    {
        return ResponseEntity.ok(taskService.findAllUncompletedTask());
    }

    /**
     * Methode qui va nous servir à ajouter une nouvelle tâche à la liste
     * @param task
     * @return 200 si la tâche a bien été ajoutée
     */
    @PostMapping("/addtask")
    public ResponseEntity<Task> create(@RequestBody Task task)
    {
        return ResponseEntity.ok(taskService.create(task));
    }

    /**
     * Methode qui va nous servir à mettre à jour une tâche
     * @param id
     * @return 200 si la tâche a bien été mise à jour
     */
    @PutMapping("/{id}")
    public ResponseEntity<Task> update(@PathVariable Long id, @RequestBody Task task)
    {
        task.setId(id);
        return ResponseEntity.ok(taskService.update(task));
    }

    /**
     * Methode qui va nous servir à supprimer une tâche
     * @param id
     * @return 200 si la tâche a bien été supprimée
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable Long id)
    {
        Task taskToDelete = taskService.findTaskById(id);
        taskService.delete(taskToDelete);
        return ResponseEntity.ok(true);
    }

}
