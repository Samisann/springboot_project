package com.todo.todolist.repositories;

import com.todo.todolist.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    public List<Task> findTaskByCompleteTrue();
    public List<Task> findTaskByCompleteFalse();

    public List<Task> findAll();

    public Task getById(Long Id);
}
