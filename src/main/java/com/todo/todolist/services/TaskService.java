package com.todo.todolist.services;

import com.todo.todolist.models.Task;
import com.todo.todolist.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Classe qui va nous servir à gérer les tâches
 * @author Sami
 */
@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    /**
     * Methode qui va nous servir à créer une tâche
     * @param task
     * @return la tâche créée
     */
    public Task create(Task task)
    {
        try{
           return taskRepository.save(task);
        }catch (Exception e)
        {
            throw new TaskException("Failed to create");
        }

    }

    /**
    * Methode qui va nous servir à récupérer toutes les tâches
    * @return une liste de tâches
    */
    public List<Task> getTasks()
    {
        return taskRepository.findAll();
    }

    /**
    * Methode qui va nous servir à récupérer une tâche par son id
    * @throws TaskNotFoundException si la tâche n'existe pas
    * @param id
    * @return la tâche
    */
    public Task findTaskById(Long id)
    {
        Optional<Task> task = taskRepository.findById(id);

        if(task.isPresent())
        {
          return task.get();
        }else
        {
            throw new TaskNotFoundException("Task not found");
        }
    }

    /**
     * Methode qui va nous servir à récupérer toutes les tâches complétées
     * @return une liste de tâches complétées
     */
    public List<Task> findAllCompletedTask()
    {
        try {
            return taskRepository.findTaskByCompleteTrue();
        }catch(Exception e)
        {
            throw new TaskNotFoundException("Tasks not found");
        }
    }

   /**
    * Methode qui va nous servir à récupérer toutes les tâches non complétées
    * @return une liste de tâches non complétées
    */  
    public List<Task> findAllUncompletedTask()
    {
        try {
            return taskRepository.findTaskByCompleteFalse();
        }catch(Exception e)
        {
            throw new TaskNotFoundException("Tasks not found");
        }
    }

    /**
     * Methode qui va nous servir à supprimer une tâche
     * @param task
     */
    public void delete(Task task)
    {
        try{
            taskRepository.delete(task);
        }catch (Exception e)
        {
            throw new TaskException("Failed to delete");
        }

    }

    /**
     * Methode qui va nous servir à mettre à jour une tâche
     * @param task
     * @return la tâche mise à jour
     */
    public Task update(Task task)
    {
        try{
            return taskRepository.save(task);
        }catch (Exception e)
        {
            throw new TaskException("Failed to update");
        }
    }


    public class TaskNotFoundException extends RuntimeException {
        public TaskNotFoundException(String message) {
            super(message);
        }
    }

    public class TaskException extends RuntimeException {
        public TaskException(String message) {
            super(message);
        }
    }


}
