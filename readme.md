# Todolist App

## Description
Cette API nous permet la de gestion de tâches permettant aux utilisateurs de créer, visualiser, mettre à jour et supprimer des tâches.

## Fonctionnalités
- **Créer une tâche:** Permet aux utilisateurs de créer une nouvelle tâche avec un titre, une description et un statut.
- **Visualiser les tâches:** Affiche la liste des tâches existantes avec leur état.
- **Mettre à jour une tâche:** Permet de modifier l'état d'une tâche existante.
- **Supprimer une tâche:** Permet de supprimer une tâche existante de la liste.

## Prérequis
- Java 17 ou supérieur

## Installation
1. Clonez ce dépôt sur votre machine locale.
2. Assurez-vous que les prérequis sont installés.
4. Lancez l'application à l'aide de votre IDE.

## Utilisation
- Accédez à l'URL suivante pour accéder à l'interface Swagger: `http://localhost:8080/swagger-ui.html`

### Endpoints disponibles
- `GET api/v1/tasks`: Récupère la liste des tâches.
- `GET api/v1/taskUncompleted`: Récupère la liste des tâches non terminées.
- `GET api/v1/taskCompleted`: Récupère la liste des tâches terminées.
- `POST api/v1/addtask`: Crée une nouvelle tâche.
- `PUT api/v1/{id}`: Met à jour l'état d'une tâche spécifique.
- `DELETE api/v1/{id}`: Supprime une tâche spécifique.

## Documentation de l'API
Swagger est utilisé pour documenter et tester l'API. Accédez à l'interface Swagger via `localhost:8080/swagger-ui.html` pour voir la documentation complète des endpoints disponibles, les paramètres requis et les requêtes.

### Accès à l'interface Swagger
Accédez à l'URL suivante pour consulter et tester les différentes fonctionnalités de l'API via Swagger: `http://localhost:8080/swagger-ui.html`

### Endpoints Disponibles

#### `GET /tasks`
- **Description:** Récupère la liste de toutes les tâches.
- **Paramètres:** Aucun.
- **Réponse:** Retourne un tableau JSON contenant toutes les tâches existantes avec leurs détails, y compris leur état.

#### `POST /addtask`
- **Description:** Crée une nouvelle tâche.
- **Paramètres:** Les détails de la nouvelle tâche à créer (titre, description).
- **Réponse:** Retourne la tâche créée.

#### `PUT /{id}`
- **Description:** Met à jour l'état d'une tâche spécifique.
- **Paramètres:** `/{id}` est l'identifiant de la tâche à mettre à jour.
- **Données Requises:** Les nouvelles données de la tâche pour la mettre à jour (par exemple, son état).
- **Réponse:** Retourne la tâche mise à jour.

#### `DELETE /{id}`
- **Description:** Supprime une tâche spécifique.
- **Paramètres:** `/{id}` est l'identifiant de la tâche à supprimer.
- **Réponse:** Retourne un booléen indiquant si la tâche a été supprimée ou non.

### Exemples

#### Créer une nouvelle tâche
- **Méthode:** `POST /addtask`
- **Corps de la Requête:**
  ```json
  {
    "id": 2,
    "task": "do sports",
    "description": "Description for qsfdcsTask 1",
    "complete": false
  }
